# Simple JS Identicons
<p align="center">
  <img src="https://camo.githubusercontent.com/697eb1aba74fe6bfc895d9d3c1aaca80f5eccda53995526fe11ffde10b64de69/687474703a2f2f692e696d6775722e636f6d2f67326c574938722e706e67" width="250"/>
</p>
<br>
<p align="center">Simple dynamically sized identicons which are randomly generated, made with p5.js</p>

## options
* **Size** - Number of "pixels" per column/row, default is 6 but you can change it from 6-9.
* **Margin** - Measured in decimal fraction, default is 0.1 (10% of pixel size).
* **Background** - Background color of the sprite, default is 240 (rgb).
* **Color** - Color of each pixel (Randomized)
